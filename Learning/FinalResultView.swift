//
//  FinalResultView.swift
//  Learning
//
//  Created by Tam Ho on 11/10/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
protocol FinalResultViewDelegate {
    func didSelectItem(index:Int)
}
extension FinalResultView:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayExerciseTypes.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:FinalResultCollectionViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "FinalResultCollectionViewCell", for: indexPath) as? FinalResultCollectionViewCell
        cell?.awakeFromNib()
        cell?.labelName.text = arrayExerciseTypes[indexPath.row].exerciseName
        
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.width - 16)/4 ,height: collectionView.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        delegate?.didSelectItem(index: arrayExerciseTypes[indexPath.row].id!)
        self.removeFromSuperview()
    }
    
}
class FinalResultView: UIView {

    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var contentView: UIView!
    
    var delegate:FinalResultViewDelegate? = nil
    var arrayResult:[Bool] = []
    var arrayExerciseTypes:[ExerciseType] = []
    var idGrammar:Int!
    var idExerciseType:Int!

    override func awakeFromNib() {
        self.width = SCREEN_WIDTH
        self.height = THUltils.setupSize(size: self.height)
    }
    func setupView()
    {
        customizeCollectionView()
        setupData()
        addCheckBoxView()
    }
    func setupData()
    {
        if let arrayExerciseTypes = ExerciseType.getExceptExercise(idGrammar:idGrammar,idExerciseType: idExerciseType)
        {
            self.arrayExerciseTypes = arrayExerciseTypes
            self.collectionView.reloadData()
        }
        
    }
    
    func customizeCollectionView()
    {
        self.collectionView.register(UINib(nibName: "FinalResultCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FinalResultCollectionViewCell")
        self.collectionView.backgroundColor = UIColor.clear
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    func addCheckBoxView()
    {
        let width:CGFloat = 50
        let constantLeft = self.collectionView.left - 50
        let constantTop = labelTitle.bottom
        var top = constantTop
        var left = constantLeft
        for (index,item) in arrayResult.enumerated()
        {
            let checkView = checkBoxView.loadFromNibNamed("checkBoxView") as! checkBoxView
            checkView.checkBox.isUserInteractionEnabled = false
            checkView.labelNumber.text = (index + 1).description
            checkView.setupView()
            checkView.checkBox.setOn(item, animated: true)
            checkView.top = top
            checkView.left = left
            if checkView.right > self.contentView.right - MARGIN_SIZE {
                
                left = constantLeft
                top = checkView.bottom + MARGIN_SIZE
                
            }
            else
            {
                left += width
            }
            checkView.top = top
            checkView.left = left
            self.contentView.addSubview(checkView)
            //            checkView.checkBox.checkboxValueChangedBlock = {
            //                isOn in
            //                print("Custom checkbox is \(isOn ? "ON" : "OFF")")
            //            }
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
