//
//  VocabularyViewController.swift
//  Learning
//
//  Created by Tam Ho on 11/2/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
extension VocabularyViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if shouldShowSearchResults {
            return filteredArray.count
        }
        else {
            return arrayCategories.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell: VocabularyCollectionViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "VocabularyCollectionViewCell", for: indexPath) as? VocabularyCollectionViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("VocabularyCollectionViewCell", owner: nil, options: nil)?[0] as? VocabularyCollectionViewCell
            
        }
        cell?.awakeFromNib()
        if shouldShowSearchResults {
            cell?.imageVol.image = UIImage(named: filteredArray[indexPath.row].imageName!)
            cell?.labelTitle.text = filteredArray[indexPath.row].content
        }
        else {
            cell?.imageVol.image = UIImage(named: arrayCategories[indexPath.row].imageName!)
            cell?.labelTitle.text = arrayCategories[indexPath.row].content
        }
        
        
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.width - MARGIN_SIZE * 3)/2 , height: (collectionView.width - MARGIN_SIZE * 3)/2)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(MARGIN_SIZE, MARGIN_SIZE, MARGIN_SIZE, MARGIN_SIZE)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return MARGIN_SIZE
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return MARGIN_SIZE
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vcWord = WordsViewController(nibName: "WordsViewController", bundle: nil)
        if(filteredArray.count == 0)
        {
            vcWord.volCategory = self.arrayCategories[indexPath.row]
        }
        else
        {
            vcWord.volCategory = self.filteredArray[indexPath.row]
        }
        self.navigationController?.pushViewController(vcWord, animated: true)
    }
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        let headerSearchbar = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "SearchCollectionReusableView", for: indexPath)
//        headerSearchbar.frame = customSearchController.customSearchBar.frame
//        headerSearchbar.addSubview(customSearchController.customSearchBar)
//        return headerSearchbar
//        
//    }
}
extension VocabularyViewController:CustomSearchControllerDelegate,UISearchResultsUpdating
{
    
    // MARK: UISearchResultsUpdating delegate function
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchString = searchController.searchBar.text else {
            return
        }
        
        // Filter the data array and get only those countries that match the search text.
        filteredArray = arrayCategories.filter({ (item:VolCategory) -> Bool in
            let content:NSString = NSString(string: item.content!)
            return (content.range(of: searchString, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        print(filteredArray)
        // Reload the tableview.
    }
    
    
    // MARK: CustomSearchControllerDelegate functions
    
    func didStartSearching() {
        shouldShowSearchResults = true
        collectionView.reloadData()

    }
    
    
    func didTapOnSearchButton() {
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
        }
        collectionView.reloadData()

    }
    
    
    func didTapOnCancelButton() {
        shouldShowSearchResults = false
        collectionView.reloadData()

    }
    
    
    func didChangeSearchText(_ searchText: String) {
        // Filter the data array and get only those countries that match the search text.
//        filteredArray = dataArray.filter({ (country) -> Bool in
//            let countryText: NSString = country as NSString
//            
//            return (countryText.range(of: searchText, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
//        })
        filteredArray = arrayCategories.filter({ (item:VolCategory) -> Bool in
            let content:NSString = NSString(string: item.content!)
            return (content.range(of: searchText, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        print(filteredArray)

        // Reload the tableview.
        collectionView.reloadData()
    }
}
class VocabularyViewController: UIViewController {

    
    @IBOutlet weak var collectionView: UICollectionView!
    var arrayCategories:[VolCategory] = []
    var dataArray = [String]()
    
    var filteredArray = [VolCategory]()
    
    var shouldShowSearchResults = false
    
    
    var customSearchController: CustomSearchController!
    
    
    init() {
        super.init(nibName: "VocabularyViewController", bundle: nil)
    }
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupData()
        
        // Do any additional setup after loading the view.
    }
    func setupView()
    {
        configureCustomSearchController()
        self.collectionView.register(UINib(nibName: "VocabularyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "VocabularyCollectionViewCell")
//        self.collectionView.register(UINib(nibName: "SearchCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "SearchCollectionReusableView")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.bounces = false
        self.collectionView.backgroundColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = true
    }
    func setupData()
    {
        if let arrayItem = VolCategory.getAll()
        {
            arrayCategories = arrayItem
            collectionView.reloadData()
            
        }
    }
    func configureCustomSearchController() {
        customSearchController = CustomSearchController(searchResultsController: self, searchBarFrame: CGRect(x: 0, y: 0, width: 0 , height: 0), searchBarFont: UIFont(name: "Futura", size: 16.0)!, searchBarTextColor: UIColor.orange, searchBarTintColor: UIColor.white)
        customSearchController.customSearchBar.backgroundColor = UIColor.white
        customSearchController.customSearchBar.placeholder = "Your Favourites"
        customSearchController.customDelegate = self
        customSearchController.customSearchBar.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.customSearchController.customSearchBar)

        let leadingConstraint = NSLayoutConstraint(item: customSearchController.customSearchBar, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.leading , multiplier: 1, constant: 0)
        
        let trailingConstraint = NSLayoutConstraint(item: customSearchController.customSearchBar, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.trailing , multiplier: 1, constant: 0)
        
        let topConstraint = NSLayoutConstraint(item: customSearchController.customSearchBar, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 44)
        
        let widthConstraint = NSLayoutConstraint(item: customSearchController.customSearchBar, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0)
        
        let heightConstraint = NSLayoutConstraint(item: customSearchController.customSearchBar, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 50)
        NSLayoutConstraint.activate([widthConstraint,heightConstraint,leadingConstraint,trailingConstraint,topConstraint])
        
        

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
