//
//  GrammarViewController.swift
//  Learning
//
//  Created by Tam Ho on 11/2/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
extension GrammarViewController:GrammarCollectionViewCellDelegate
{
    func didSelectRow(grammar:Grammar)
    {
        let introController = IntroductionViewController()
        introController.grammarModel = grammar
        self.navigationController?.pushViewController(introController, animated: true)
    }
}
extension GrammarViewController: MenuViewDelegate
{
    func actionAfterPress(sender:UIButton) {
        previousIndex = selectedIndex
        selectedIndex = sender.tag
        arrayMenuView[previousIndex].setLineColor(selected: false)
        arrayMenuView[selectedIndex].setLineColor(selected: true)
        if(selectedIndex > previousIndex)
        {
            self.collectionView.scrollToItem(at: IndexPath(row: selectedIndex, section: 0), at: UICollectionViewScrollPosition.right, animated: true);

        }
        else
        {
            self.collectionView.scrollToItem(at: IndexPath(row: selectedIndex, section: 0), at: UICollectionViewScrollPosition.left, animated: true);

        }

        
    }
}
extension GrammarViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayMenuView.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        var cell: GrammarCollectionViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "GrammarCollectionViewCell", for: indexPath) as? GrammarCollectionViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("GrammarCollectionViewCell", owner: nil, options: nil)?[0] as? GrammarCollectionViewCell
            
        }
        cell?.type = arrayMenu[indexPath.row].id
        cell?.delegate = self
        cell?.awakeFromNib()
        
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width , height: collectionView.height)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        
        let pageNumber = round(scrollView.contentOffset.x / collectionView.frame.size.width)
        previousIndex = selectedIndex
        selectedIndex = Int(pageNumber)
        self.lastContentOffset = scrollView.contentOffset.x
        arrayMenuView[previousIndex].setLineColor(selected: false)
        arrayMenuView[selectedIndex].setLineColor(selected: true)
    }
    
}
class GrammarViewController: UIViewController {

    @IBOutlet weak var scrollBar: UIScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    var arrayMenuView:[MenuView] = []
    var lastContentOffset:CGFloat = 0
    var arrayMenu:[Type] = []
    var selectedIndex = 0
    var previousIndex = 0
    init() {
        super.init(nibName: "GrammarViewController", bundle: nil)
    }
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        // Do any additional setup after loading the view.
    }
    func setupView()
    {
        self.collectionView.register(UINib(nibName: "GrammarCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GrammarCollectionViewCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.bounces = false
        self.collectionView.backgroundColor = UIColor.black
        self.navigationController?.isNavigationBarHidden = true
        setupData()
        
    }
    func setupData()
    {
        if let arrayMenu = Type.getTypebyIdSkill(idSkill: GRAMMAR)
        {
            self.arrayMenu = arrayMenu
            setupMenuView()
            arrayMenuView[selectedIndex].setLineColor(selected: true)
            self.collectionView.reloadData()
        }
    }
    func setupMenuView()
    {
        var totalWidth:CGFloat = 0
        print(SCREEN_WIDTH)
        let widthBar:CGFloat = SCREEN_WIDTH/CGFloat(arrayMenu.count)
        for (index,item) in arrayMenu.enumerated()
        {
            
            let barItem = MenuView.loadFromNibNamed("MenuView") as! MenuView
            barItem.delegate = self
            barItem.setupView(width: widthBar, height: scrollBar.height)
            barItem.setText(menuName: item.typeName!,index: index)
            
            barItem.left = totalWidth
            totalWidth += widthBar
            scrollBar.addSubview(barItem)
            arrayMenuView.append(barItem)
        }
        scrollBar.contentSize.width = totalWidth
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
