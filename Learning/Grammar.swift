//
//  GrammarModel.swift
//  Learning
//
//  Created by Tam Ho on 11/2/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import Foundation

class Grammar: SQLTable {
    
    var id:Int?
    var nameLesson:String?
    var summaryContent:String?
    var idType:Int?
    init(nameLesson:String,summaryContent:String,idType:Int)
    {
        self.nameLesson = nameLesson
        self.idType = idType
    }
    init(id:Int,nameLesson:String,summaryContent:String,idType:Int)
    {
        self.id = id
        self.summaryContent = summaryContent
        self.nameLesson = nameLesson
        self.idType = idType
    }
    init(nameLesson:String,idType:Int)
    {
        self.nameLesson = nameLesson
        self.summaryContent = ""
        self.idType = idType
    }
    static func getLessonbyIDType(idType:Int) -> [Grammar]?
    {
        
        let rows = self.rows(filter: "idType = \(idType)")
        var arrayGrammar:[Grammar] = []
        for item in rows
        {
            
            let id = item["id"] as? Int
            let nameLesson = item["nameLesson"] as? String
            let summaryContent = item["summaryContent"] as? String
            let idType = item["idType"] as? Int

            guard id != nil && summaryContent != nil && idType != nil else {
                return nil
            }
            var model = Grammar(id: id!, nameLesson: nameLesson!, summaryContent: summaryContent!, idType: idType!)
            arrayGrammar.append(model)
        }
        return arrayGrammar
    }
    
    required init() {
        
    }
}
