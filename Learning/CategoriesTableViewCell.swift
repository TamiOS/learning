//
//  CategoriesTableViewCell.swift
//  Learning
//
//  Created by Tam Ho on 11/8/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
protocol CategoriesTableViewCellDelegate {
    func didSelectExercise(index:Int)
}
class CategoriesTableViewCell: UITableViewCell {

    
    @IBOutlet weak var buttonCategories: UIButton!
    var delegate:CategoriesTableViewCellDelegate? = nil
    
    @IBAction func chooseExercise(_ sender: UIButton) {
        
        delegate?.didSelectExercise(index: sender.tag)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
