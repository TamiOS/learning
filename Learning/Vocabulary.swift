//
//  Vocabulary.swift
//  Learning
//
//  Created by Tam Ho on 11/30/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import Foundation
class Vocabulary:SQLTable
{
    var id:Int?
    var content:String?
    var type:Int?
    var image:String?
    var pronun:String?
    var fav:Int?
    init(id:Int,content:String,type:Int,image:String,pronun:String,fav:Int) {
        self.id = id
        self.content = content
        self.type = type
        self.image = image
        self.pronun = pronun
        self.fav = fav
    }
    required init() {
        
    }
    static func getbyID(type:Int) -> [Vocabulary]?
    {
        var arrayVol:[Vocabulary] = []
        let rows = self.rows(filter: "type = \(type)")
        for item in rows
        {
            let id = item["id"] as? Int
            let content = item["content"] as? String
            let type = item["type"] as? Int
            let image = item["image"] as? String
            let pronun = item["pronun"] as? String
            let fav = item ["fav"] as? Int
            
            guard id != nil && image != nil && content != nil && type != nil
                else {
                    return nil
            }
            let model = Vocabulary(id: id!, content: content!, type: type!, image: image!,pronun: pronun!,fav:fav!)
            arrayVol.append(model)
        }
        return arrayVol
    }
    static func getFavourites(type:Int) -> [Vocabulary]?
    {
        var arrayVol:[Vocabulary] = []
        let rows = self.rows(filter: "type = \(type) and fav = 1")
        for item in rows
        {
            let id = item["id"] as? Int
            let content = item["content"] as? String
            let type = item["type"] as? Int
            let image = item["image"] as? String
            let pronun = item["pronun"] as? String
            let fav = item ["fav"] as? Int
            
            guard id != nil && image != nil && content != nil && type != nil
                else {
                    return nil
            }
            let model = Vocabulary(id: id!, content: content!, type: type!, image: image!,pronun: pronun!,fav:fav!)
            arrayVol.append(model)
        }
        return arrayVol
    }
    func setFavourite(completion:(_ success:Bool) -> Void)
    {
        
        if(self.update(data: ["fav" : 1], id: self.id!))
        {
            completion(true)
        }
        else
        {
            completion(false)
        }
    }
    func removeFavourite(completion:(_ success:Bool) -> Void)
    {
        if(self.update(data: ["fav" : 0], id: self.id!))
        {
            completion(true)
        }
        else
        {
            completion(false)
        }
    }
}
