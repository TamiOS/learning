//
//  HeaderView.swift
//  Learning
//
//  Created by Tam Ho on 11/2/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
protocol HeaderViewDelegate {
    func actionAfterBack()
}
class HeaderView: BaseView {

    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var labelHeaderName: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func setupView()
    {
        width = SCREEN_WIDTH
        height = 44
        self.containerView.width = self.width
        self.containerView.height = self.height
        labelHeaderName.width = self.width
        labelHeaderName.height = self.height
        THUltils.setupSize(subView: [buttonBack])
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.black.cgColor
        print(width)
        print(height)
    }
    func setText(text:String,isHidden:Bool)
    {
        
        
        labelHeaderName.text = text
        labelHeaderName.sizeThatFits(CGSize(width: SCREEN_WIDTH, height: labelHeaderName.height))
        labelHeaderName.top = self.height/2 - labelHeaderName.height/2
        labelHeaderName.left = self.width/2 - labelHeaderName.width/2
        buttonBack.isHidden = isHidden
    }
    @IBAction func actionBack(_ sender: UIButton) {
        popViewController()
    }

    
}
