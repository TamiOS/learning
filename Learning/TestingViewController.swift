//
//  TestingViewController.swift
//  Learning
//
//  Created by Tam Ho on 11/2/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit

class TestingViewController: UIViewController {

    
    @IBOutlet weak var collectionView: UICollectionView!
    
    init() {
        super.init(nibName: "TestingViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
