//
//  ExerciseType.swift
//  Learning
//
//  Created by Tam Ho on 11/9/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import Foundation
class ExerciseType: SQLTable {
    
    var id:Int?
    var exerciseName:String?
    var idGrammar:Int?
    required init() {
        
    }
    init(id:Int,exerciseName:String,idGrammar:Int) {
        
        self.id = id
        self.exerciseName = exerciseName
        self.idGrammar = idGrammar
    }
    static func getbyIDGrammar(idGrammar:Int) -> [ExerciseType]?
    {
        var arrayExerciseTypes:[ExerciseType] = []
        let rows = self.rows(filter: "idGrammar = \(idGrammar)")
        for item in rows
        {
            let id = item["id"] as? Int
            let exerciseName = item["exerciseName"] as? String
            let idGrammar = item["idGrammar"] as? Int
            
            guard id != nil && exerciseName != nil && idGrammar != nil
                else {
                return nil
            }
            let model = ExerciseType(id: id!, exerciseName: exerciseName!, idGrammar: idGrammar!)
            arrayExerciseTypes.append(model)
        }
        
        return arrayExerciseTypes
    }
    static func getExceptExercise(idGrammar:Int,idExerciseType:Int) -> [ExerciseType]?
    {
        var arrayExerciseTypes:[ExerciseType] = []
        let rows = self.rows(filter: "idGrammar = \(idGrammar) and id <> \(idExerciseType)")
        for item in rows
        {
            let id = item["id"] as? Int
            let exerciseName = item["exerciseName"] as? String
            let idGrammar = item["idGrammar"] as? Int
            
            guard id != nil && exerciseName != nil && idGrammar != nil
                else {
                    return nil
            }
            let model = ExerciseType(id: id!, exerciseName: exerciseName!, idGrammar: idGrammar!)
            arrayExerciseTypes.append(model)
        }
        
        return arrayExerciseTypes
    }
}
