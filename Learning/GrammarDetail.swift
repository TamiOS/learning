//
//  GrammarDetail.swift
//  Learning
//
//  Created by Tam Ho on 11/3/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import Foundation
class GrammarDetail: SQLTable {
    
    var id:Int?
    var content:String?
    var idGrammar:Int?
    init(content:String,idGrammar:Int) {
        
        self.content = content
        self.idGrammar = idGrammar
        
    }
    init(idGrammar:Int) {
        
        self.idGrammar = idGrammar
    }
    required init() {
        
    }
    static func getContent(idGrammar:Int) -> GrammarDetail?
    {
        let rows = self.rows(filter: "idGrammar = \(idGrammar)")
        let model = GrammarDetail()
        let id = rows[0]["id"] as? Int
        let content = rows[0]["content"] as? String
        let idGrammar = rows[0]["idGrammar"] as? Int
        
        guard id != nil && content != nil && idGrammar != nil else {
            return nil
        }
        model.id = id
        model.content = content
        model.idGrammar = idGrammar
        return model
    }
}
