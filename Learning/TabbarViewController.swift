//
//  TabbarViewController.swift
//  Learning
//
//  Created by Tam Ho on 11/1/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit

class TabbarViewController: BaseViewController {

    
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var containView: UIView!
    @IBOutlet var buttons: [UIButton]!
    var grammarController:GrammarViewController!
    var testingController:TestingViewController!
    var vocabularyController:VocabularyViewController!
    var settingController:SettingViewController!
    var homeController:HomeViewController!
    var viewControllers:[UIViewController]!
    var selectedIndex:Int = 1
    var previousIndex:Int = 0
    var vc:UIViewController!
    init() {
        super.init(nibName: "TabbarViewController", bundle: nil)
    }
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        

        // Do any additional setup after loading the view.
    }
    func setupView()
    {
        self.navigationController?.isNavigationBarHidden = true
        grammarController = GrammarViewController()
        testingController = TestingViewController()
        vocabularyController = VocabularyViewController()
        homeController = HomeViewController()
        viewControllers = [homeController,grammarController,vocabularyController,testingController]
        vc = viewControllers[selectedIndex]
        buttons[selectedIndex].isSelected = true
        lineView.backgroundColor = UIColor.black
        didPressTab(buttons[selectedIndex])
        addHeaderView(text: (buttons[selectedIndex].titleLabel?.text!)!, isHidden: true)
        for item in buttons
        {
            item.titleEdgeInsets = UIEdgeInsetsMake(15, 0, 0, 0)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func didPressTab(_ sender: UIButton) {
        if(sender.tag == 0)
        {
            (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = HomeViewController()
        }
        previousIndex = selectedIndex
        selectedIndex = sender.tag
        buttons[previousIndex].isSelected = false
        let previousVC = viewControllers[previousIndex]
        previousVC.willMove(toParentViewController: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParentViewController()
        sender.isSelected = true
        vc = viewControllers[selectedIndex]
        addChildViewController(vc)
        self.headerView.labelHeaderName.text = buttons[selectedIndex].titleLabel?.text
        viewWillAppear(true)
        viewDidAppear(true)
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addChildViewController(vc)
        vc.view.frame = containView.bounds
        containView.addSubview(vc.view)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        vc.didMove(toParentViewController: self)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
