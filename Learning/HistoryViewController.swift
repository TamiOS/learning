//
//  HistoryViewController.swift
//  Learning
//
//  Created by Tam Ho on 12/11/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit

class HistoryViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addHeaderView(text: "History", isHidden: false)

        // Do any additional setup after loading the view.
    }
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
