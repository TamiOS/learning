//
//  CustomSegmentViewController.swift
//  Learning
//
//  Created by Tam Ho on 11/5/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit

class CustomSegmentViewController: BaseViewController {

    
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet var buttons: [UIButton]!
    
    var tabGrammar:TabGrammarViewController!
    var tabExercise:TabExerciseViewController!
    var viewControllers:[UIViewController]!
    var selectedIndex = 0
    var previousIndex = 0
    var vc:UIViewController!
    var idGrammar:Int!
    init(idGrammar:Int) {
        
        super.init(nibName: "CustomSegmentViewController", bundle: nil)
        
        self.idGrammar = idGrammar
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override var prefersStatusBarHidden: Bool{
        return true
    }
    @IBAction func didPress(_ sender: UIButton) {
        
        previousIndex = selectedIndex
        selectedIndex = sender.tag
        buttons[previousIndex].isSelected = false
        let previousVC = viewControllers[previousIndex]
        previousVC.willMove(toParentViewController: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParentViewController()
        sender.isSelected = true
        vc = viewControllers[selectedIndex]
        addChildViewController(vc)
        self.headerView.labelHeaderName.text = buttons[selectedIndex].titleLabel?.text
        viewWillAppear(true)
        viewDidAppear(true)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addChildViewController(vc)
        vc.view.frame = contentView.bounds
        print(vc.view.frame)
        contentView.addSubview(vc.view)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        vc.didMove(toParentViewController: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Do any additional setup after loading the view.
    }
    func setupView()
    {
        tabGrammar = TabGrammarViewController(idGrammar: self.idGrammar)
        tabExercise = TabExerciseViewController(idGrammar: self.idGrammar)
        viewControllers = [tabGrammar,tabExercise]
        vc = viewControllers[selectedIndex]
        buttons[selectedIndex].isSelected = true
        didPress(buttons[selectedIndex])
        addHeaderView(text: (buttons[selectedIndex].titleLabel?.text!)!, isHidden: false)
    }
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
