//
//  DropDownView.swift
//  Learning
//
//  Created by Tam Ho on 11/8/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
protocol DropDownViewDelegate {
    func didPressChange()
}
class DropDownView: UIView {

    var delegate:DropDownViewDelegate? = nil
    @IBOutlet weak var buttonCategories: UIButton!
    
    @IBAction func pressChange(_ sender: UIButton) {
        
        delegate?.didPressChange()
    }
    
    override func awakeFromNib() {
    }
    
        /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
