//
//  GrammarCollectionViewCell.swift
//  Learning
//
//  Created by Tam Ho on 11/3/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
protocol GrammarCollectionViewCellDelegate {
    func didSelectRow(grammar:Grammar)
}
extension GrammarCollectionViewCell:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayGrammar.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: GrammarTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell") as? GrammarTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("GrammarTableViewCell", owner: nil, options: nil)?[0] as? GrammarTableViewCell
            
        }
        cell?.width = SCREEN_WIDTH
        cell?.height = THUltils.setupSize(size: 64)
        cell?.awakeFromNib()
        cell?.labelLession.text = arrayGrammar[indexPath.row].nameLesson

        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return THUltils.setupSize(size: widthCell)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        delegate?.didSelectRow(grammar: arrayGrammar[indexPath.row])
        
        
    }
    
}
class GrammarCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var tableView: UITableView!
    var widthCell:CGFloat = 64
    var arrayGrammar:[Grammar] = []
    var type:Int! = 1
    var delegate:GrammarCollectionViewCellDelegate? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.setupData()
        // Initialization code
    }
    func setupData()
    {
        self.arrayGrammar = Grammar.getLessonbyIDType(idType: type)!
        self.tableView.reloadData()
    }
    

}
