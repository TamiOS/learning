//
//  HomeViewController.swift
//  Learning
//
//  Created by Tam Ho on 12/10/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var buttonStart: UIButton!
    
    @IBOutlet weak var buttonHistory: UIButton!
    
    @IBOutlet weak var buttonMail: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Do any additional setup after loading the view.
    }
    func setupView()
    {
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionStart(_ sender: UIButton) {
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = UINavigationController(rootViewController: TabbarViewController())
    }

    @IBAction func actionHistory(_ sender: UIButton) {
        
    let controller = HistoryViewController(nibName: "HistoryViewController", bundle: nil)
    self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func actionMail(_ sender: UIButton) {
        
        let controller = SettingViewController(nibName: "SettingViewController", bundle: nil)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
