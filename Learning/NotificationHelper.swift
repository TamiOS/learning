//
//  NotificationHelper.swift
//  FrenchLearning
//
//  Created by Tam Ho on 10/28/16.
//  Copyright © 2016 LeeNguyen. All rights reserved.
//

import Foundation
protocol NotificationHelperDelegate {
    
    func reactOnNotification(_ notification:Foundation.Notification)
}
class NotificationHelper:NotificationHelperDelegate
{
    var delegate:NotificationHelperDelegate? = nil
    static func pushNotification(_ nameNotification:String,withObject:AnyObject)
    {
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: nameNotification), object: withObject)
    }
    static func registerForNotification(_ notification:String,withDelegate:AnyObject)
    {
        NotificationCenter.default.addObserver(withDelegate, selector: #selector(NotificationHelper.reactOnNotification(_:)), name: NSNotification.Name(rawValue: notification), object: nil)
    }
    static func unregisterForNotification(_ delegate:AnyObject)
    {
        NotificationCenter.default.removeObserver(delegate)
    }
    @objc func reactOnNotification(_ notification: Foundation.Notification) {
        
        delegate?.reactOnNotification(notification)
    }
}
