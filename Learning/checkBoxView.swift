//
//  checkBoxView.swift
//  Learning
//
//  Created by Tam Ho on 11/10/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit

class checkBoxView: UIView {
    

    
    @IBOutlet weak var checkBox: VKCheckbox!
    
    @IBOutlet weak var labelNumber: UILabel!
    
    override func awakeFromNib() {
        
        setupView()
    }
    func setupView()
    {
        self.width = THUltils.setupSize(size: self.width)
        self.height = THUltils.setupSize(size: self.height)
        self.labelNumber.font = UIFont(name: self.labelNumber.font.fontName, size: THUltils.setupSize(size: self.labelNumber.font.pointSize))
        checkBox.line             = .thin
        checkBox.bgColorSelected  = UIColor(red: 46/255, green: 119/255, blue: 217/255, alpha: 1)
        checkBox.bgColor          = UIColor.gray
        checkBox.color            = UIColor.white
        checkBox.borderColor      = UIColor.white
        checkBox.borderWidth      = 2
        checkBox.cornerRadius     = checkBox.frame.height / 2
        
        // Handle custom checkbox callback
        checkBox.checkboxValueChangedBlock = {
            isOn in
            print("Custom checkbox is \(isOn ? "ON" : "OFF")")
        }

    }
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
