//
//  TabExerciseViewController.swift
//  Learning
//
//  Created by Tam Ho on 11/6/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
extension TabExerciseViewController:FinalResultViewDelegate
{
    func didSelectItem(index: Int) {
        reloadData(idExercise: index)
    }
}
extension TabExerciseViewController : FillQuestionViewDelegate
{
    func didPressOKFill()
    {
        checkFinishing()
    }
}
extension TabExerciseViewController : AnswerViewDelegate
{
    func didPressOK()
    {
        checkFinishing()
    }
}

class TabExerciseViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
   
    var idGrammar:Int!
    var arrayExercise = [Exercise]()
    var arrayView:[UIView] = []
    var arrayQuestion:[Bool] = []
    var arrayAnswer:[Bool] = []

    var idExerciseType = 1

    init(idGrammar:Int) {
        super.init(nibName: "TabExerciseViewController", bundle: nil)
        self.idGrammar = idGrammar
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        // Do any additional setup after loading the view.
    }
    func checkFinishing()
    {
        var indexAnswer:Int? = -1
        indexAnswer = arrayQuestion.index { (item:Bool) -> Bool in
            if(item == false)
            {
                return true
            }
            return false
        }
        if(indexAnswer != nil)
        {
            self.scrollView.setContentOffset(CGPoint(x: self.scrollView.contentOffset.x, y: CGFloat(indexAnswer!) * scrollView.height), animated: true)
        }
        else
        {
            let finalView = FinalResultView.loadFromNibNamed("FinalResultView") as! FinalResultView
            finalView.frame = self.view.frame
            finalView.delegate = self
            finalView.idExerciseType = idExerciseType
            finalView.idGrammar = idGrammar
            finalView.arrayResult = arrayAnswer
            finalView.setupView()
            self.view.addSubview(finalView)
        }

    }
    func setupData()
    {
        
        if let arrayExercise:[Exercise] = Exercise.getExercises(idGrammar: self.idGrammar,idExerciseType: self.idExerciseType)
        {
            arrayQuestion = [Bool](repeatElement(false, count: arrayExercise.count))
            arrayAnswer = [Bool](repeatElement(false, count: arrayExercise.count))

            let leftAnswerView:CGFloat = 0
            var topAnswerView:CGFloat = 0
            for (index,item) in arrayExercise.enumerated()
            {
                if(self.idExerciseType == 1)
                {
                    let answerView = AnswerView.loadFromNibNamed("AnswerView") as! AnswerView
                    
                    answerView.width = self.scrollView.width
                    answerView.height = self.scrollView.height
                    answerView.left = leftAnswerView
                    answerView.top = topAnswerView
                    topAnswerView += answerView.height
                    answerView.numberPage = arrayExercise.count
                    answerView.currentIndex = index
                    answerView.setupView(exercise: item)
                    answerView.delegate = self
                    answerView.parentViewController = self
                    arrayView.append(answerView)
                    self.scrollView.isScrollEnabled = true
                    self.scrollView.addSubview(answerView)
                }
                else if(self.idExerciseType == 2)
                {
                    let questionView = QuestionView.loadFromNibNamed("QuestionView") as! QuestionView
                    questionView.width = self.scrollView.width
                    questionView.height = self.scrollView.height
                    questionView.left = leftAnswerView
                    questionView.top = topAnswerView
                    topAnswerView += questionView.height
                    questionView.numberPage = arrayExercise.count
                    questionView.currentIndex = index
                    questionView.exercise = item
                    questionView.setupView()
                    questionView.delegate = self
                    questionView.parentViewController = self
                    arrayView.append(questionView)
                    self.scrollView.isScrollEnabled = true
                    self.scrollView.addSubview(questionView)
                }
                else
                {
                    let fillQuestion = FillQuestionView.loadFromNibNamed("FillQuestionView") as! FillQuestionView
                    fillQuestion.width = self.scrollView.width
                    fillQuestion.height = self.scrollView.height
                    fillQuestion.left = leftAnswerView
                    fillQuestion.top = topAnswerView
                    topAnswerView += fillQuestion.height
                    fillQuestion.numberPage = arrayExercise.count
                    fillQuestion.currentIndex = index
                    fillQuestion.setupView(exercise: item)
                    fillQuestion.delegate = self
                    fillQuestion.parentViewController = self
                    arrayView.append(fillQuestion)
                    self.scrollView.isScrollEnabled = true
                    self.scrollView.addSubview(fillQuestion)
                }
                
            }
            self.scrollView.contentSize.height = topAnswerView
        }
        
    }
    func reloadData(idExercise:Int)
    {
        self.idExerciseType = idExercise
        for item in arrayView
        {
            item.removeFromSuperview()
        }
        self.arrayView.removeAll()
        self.arrayExercise.removeAll()
        self.scrollView.contentOffset.y = 0
        self.scrollView.contentSize.height = 0
        self.setupData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
