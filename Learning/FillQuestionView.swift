//
//  FillQuestionView.swift
//  Learning
//
//  Created by Tam Ho on 11/20/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
extension UITextField
{
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneButtonAction))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.inputAccessoryView = doneToolbar
    }
    func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}

extension FillQuestionView : CategoriesViewDelegate
{
    func didCloseView() {
        
        parentViewController.scrollView.isScrollEnabled = true
    }
    func didSelectItem(index: Int) {
        
        parentViewController.reloadData(idExercise: index)
    }
    
}
extension FillQuestionView : DropDownViewDelegate
{
    func didPressChange() {
        
        let categoryView = CategoriesView.loadFromNibNamed("CategoriesView") as! CategoriesView
        categoryView.delegate = self
        categoryView.idGrammar = exercise.idGrammar
        categoryView.setupView()
        parentViewController.scrollView.isScrollEnabled = false
        self.addSubview(categoryView)
    }
}
extension FillQuestionView:ResultViewDelegate
{
    func didPressAgain() {
        
        let currentPage =  Int(parentViewController.scrollView.contentOffset.y / parentViewController.scrollView.height)
        textFieldFill.text = ""
        textFieldFill.textColor = UIColor.black
        parentViewController.arrayQuestion[currentPage] = false

    }
}
extension FillQuestionView:UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        UIView.animate(withDuration: KEYBOARD_ANIMATE_DURATION) {
            
            self.textFieldFill.top -= KEYBOARD_HEIGHT + 50
            self.bottomLine.top -= KEYBOARD_HEIGHT + 50
            self.btnOK.top -= KEYBOARD_HEIGHT + 50
            self.indicatorDot.top -= KEYBOARD_HEIGHT + 50
            
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        UIView.animate(withDuration: KEYBOARD_ANIMATE_DURATION) {
            
            self.textFieldFill.top += KEYBOARD_HEIGHT + 50
            self.bottomLine.top += KEYBOARD_HEIGHT + 50
            self.btnOK.top += KEYBOARD_HEIGHT + 50
            self.indicatorDot.top += KEYBOARD_HEIGHT + 50
            
        }
    }
}
protocol FillQuestionViewDelegate {
    func didPressOKFill()

}
class FillQuestionView: UIView {

    var delegate:AnswerViewDelegate? = nil
    var answer:Answer!
    var exercise:Exercise!
    var parentViewController:TabExerciseViewController!
    var currentIndex:Int = 1
    var numberPage = 1
    var textFieldFill:UITextField!
    var indicatorDot:UIPageControl!
    var bottomLine:UIView!

    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var lblQuestion: UILabel!
    
    
    @IBOutlet weak var btnOK: UIButton!
    
    
    @IBAction func doButtonOK(_ sender: UIButton) {
        
        let currentPage =  Int(parentViewController.scrollView.contentOffset.y / parentViewController.scrollView.height)
        if(checkTrueQuestion())
        {
            print("True")
            textFieldFill.textColor = UIColor.blue
            parentViewController.arrayAnswer[currentPage] = true

        }
        else
        {
            textFieldFill.textColor = UIColor.red
            parentViewController.arrayAnswer[0] = false
            let resultView = ResultView.loadFromNibNamed("ResultView") as! ResultView
            resultView.frame = self.contentView.frame
            resultView.delegate = self
            self.contentView.addSubview(resultView)
        }
        textFieldFill.endEditing(true)
        parentViewController.arrayQuestion[currentPage] = true
        delegate?.didPressOK()
    }
    func checkTrueQuestion() -> Bool
    {
        var arrayItem = answer.content?.components(separatedBy: " ")
        var arrayFill = textFieldFill.text?.components(separatedBy: " ")
        guard arrayItem != nil && arrayFill != nil else {
            return false
        }
        if(arrayItem?.count == arrayFill?.count)
        {
            for (index,_) in (arrayItem?.enumerated())!
            {
                if(arrayFill![index].lowercased() != arrayItem![index])
                {
                    return false
                }
            }
        }
        else
        {
            return false
        }
        return true
        
    }
    
    override func awakeFromNib() {
        
    }
   
    func setupView(exercise:Exercise)
    {
      

        indicatorDot = UIPageControl(frame: self.btnOK.frame)
        indicatorDot.backgroundColor = UIColor.clear
        indicatorDot.pageIndicatorTintColor = btnOK.titleLabel?.textColor
        indicatorDot.currentPageIndicatorTintColor = UIColor.gray
        indicatorDot.height = btnOK.height/2
        indicatorDot.top = btnOK.bottom
        indicatorDot.numberOfPages = numberPage
        indicatorDot.currentPage = currentIndex
        self.contentView.addSubview(indicatorDot)
        self.exercise = exercise
        textFieldFill = UITextField()
        textFieldFill.width = btnOK.width
        textFieldFill.height = btnOK.height
        textFieldFill.left = MARGIN_SIZE
        textFieldFill.bottom = btnOK.top - MARGIN_SIZE * 2
        textFieldFill.borderStyle = UITextBorderStyle.none
        textFieldFill.layer.borderWidth = 0
        textFieldFill.delegate = self
        textFieldFill.addDoneButtonOnKeyboard()
        self.contentView.addSubview(textFieldFill)
        bottomLine = UIView()
        bottomLine.width = textFieldFill.width
        bottomLine.height = 1
        bottomLine.left = textFieldFill.left
        bottomLine.top = textFieldFill.bottom
        bottomLine.backgroundColor = UIColor.black
        self.contentView.addSubview(bottomLine)
        addDropDownView()
        setupData()
    }
    func setupData()
    {
        
        if let arrayAnswers = Answer.getAnswer(idExercise: self.exercise.id!)
        {
            lblQuestion.text = exercise.contentQuestion
            lblQuestion.sizeToFit()
            lblQuestion.width = self.width - MARGIN_SIZE * 2
            customizeLabel()
            answer = arrayAnswers[0]
            
        }
    }
    func addDropDownView()
    {
        let dropDown = DropDownView.loadFromNibNamed("DropDownView") as! DropDownView
        
        dropDown.delegate = self
        dropDown.frame = self.btnOK.frame
        dropDown.layer.borderColor = UIColor.black.cgColor
        dropDown.layer.borderWidth = 1
        dropDown.layer.cornerRadius = 10
        dropDown.top = MARGIN_SIZE
        self.contentView.addSubview(dropDown)
        self.lblQuestion.top = dropDown.bottom + MARGIN_SIZE
    }
    func customizeLabel()
    {
        lblQuestion.isHidden = true
        let arrayText:[String] = (lblQuestion.text?.components(separatedBy: "_"))!
        let constantLeft = lblQuestion.left + MARGIN_SIZE
        let constantTop = lblQuestion.top + MARGIN_SIZE * 2
        
        var left = constantLeft
        var top = constantTop
        var height:CGFloat = 0
        var right:CGFloat = 0
        var bottom:CGFloat = 0
        let viewQuestion = UIView(frame: btnOK.frame)
        viewQuestion.top = lblQuestion.top
        
        self.contentView.addSubview(viewQuestion)
        
        for (index,item) in arrayText.enumerated()
        {
            
            let labelText = UILabel()
            labelText.numberOfLines = 0
            labelText.font = UIFont(name: lblQuestion.font.fontName, size: lblQuestion.font.pointSize)
            labelText.text = item
            labelText.sizeToFit()
            if(right + labelText.intrinsicContentSize.width > lblQuestion.right)
            {
                left = constantLeft
                top  = bottom
            }
            labelText.left = left
            labelText.top = top
            left = labelText.right + MARGIN_SIZE
            top = labelText.top
            self.contentView.addSubview(labelText)
            if(left + 60 >= lblQuestion.right)
            {
                left = constantLeft
                top  = bottom
            }
            if(index != arrayText.count - 1)
            {
                let spaceView = UIView()
                spaceView.layer.borderColor = UIColor.black.cgColor
                spaceView.layer.borderWidth = 1
                spaceView.layer.cornerRadius = 8
                spaceView.width = 60
                spaceView.height = labelText.height
                spaceView.top = top
                spaceView.left = left
                left = spaceView.right + MARGIN_SIZE
                self.contentView.addSubview(spaceView)
                right = spaceView.right + MARGIN_SIZE
                bottom = spaceView.bottom + MARGIN_SIZE
            }
            height = labelText.bottom
        }
        viewQuestion.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        viewQuestion.layer.cornerRadius = 10
        viewQuestion.left = MARGIN_SIZE
        viewQuestion.height = height - viewQuestion.top + MARGIN_SIZE * 2
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
