//
//  TabGrammarViewController.swift
//  Learning
//
//  Created by Tam Ho on 11/6/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
extension String {
    var html2AttributedString: NSAttributedString? {
        
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
        
    }
}
class TabGrammarViewController: UIViewController {

    @IBOutlet weak var textviewContent: UITextView!
    var idGrammar:Int!
    var grammarDetail:GrammarDetail?
    init(idGrammar:Int) {
        super.init(nibName: "TabGrammarViewController", bundle: nil)
        self.idGrammar = idGrammar
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        
        // Do any additional setup after loading the view.
    }
    func setupData()
    {
        if let grammarDetail = GrammarDetail.getContent(idGrammar: idGrammar)
        {
//            self.grammarDetail = grammarDetail
//            var stringValue = NSMutableAttributedString(attributedString: (grammarDetail.content?.html2AttributedString)!)
//            var range:NSRange = NSRange.init(location: 0, length: (stringValue.length))
//            stringValue.enumerateAttribute(NSFontAttributeName, in: range, options: NSAttributedString.EnumerationOptions.longestEffectiveRangeNotRequired, using: { (value:Any?, range:NSRange, stop:UnsafeMutablePointer<ObjCBool>) in
//                var currentFont = value
//                var font = UIFont(name: (textviewContent.font?.fontName)!, size: 18)
//                stringValue.addAttribute(NSFontAttributeName, value: font, range: range)
//                textviewContent.attributedText = stringValue
//
//            })
                textviewContent.attributedText = grammarDetail.content?.html2AttributedString

        }
        else
        {
            textviewContent.text = ""
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
