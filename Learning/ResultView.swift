//
//  ResultView.swift
//  Learning
//
//  Created by Tam Ho on 11/7/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
protocol ResultViewDelegate {
    func didPressAgain()
}
class ResultView: UIView {
    
    var delegate:ResultViewDelegate? = nil
    @IBAction func actionPlayAgain(_ sender: UIButton) {
        
        delegate?.didPressAgain()
        self.removeFromSuperview()
        
    }
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
