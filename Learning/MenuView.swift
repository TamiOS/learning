//
//  MenuView.swift
//  Learning
//
//  Created by Tam Ho on 11/2/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
protocol MenuViewDelegate {
    func actionAfterPress(sender:UIButton)
}
class MenuView: BaseView {

    
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var contentView: UIView!
    var delegate:MenuViewDelegate? = nil
    var bottomLine:UIView!
    @IBAction func actionPress(_ sender: UIButton) {
        delegate?.actionAfterPress(sender: sender)
    }
    func setupView(width:CGFloat,height:CGFloat)
    {
        self.width = width
        self.height = height
        self.backgroundColor = UIColor.clear
        self.contentView.width = width
        self.contentView.height = height
        self.contentView.left = 0
        self.contentView.top = 0
        self.contentView.backgroundColor = UIColor.clear
        self.btnTitle.width = self.contentView.width
        self.btnTitle.height = self.contentView.height - 2
        self.btnTitle.left = 0
        self.btnTitle.top = 0
        addBottomLineView()
    }
    func setText(menuName:String,index:Int)
    {
        self.btnTitle.tag = index
        self.btnTitle.backgroundColor = UIColor.clear
        self.btnTitle.setTitle(menuName, for: UIControlState.normal)
        self.btnTitle.titleLabel?.adjustsFontSizeToFitWidth = true
        self.btnTitle.titleLabel?.font = UIFont(name: (self.btnTitle.titleLabel?.font.fontName)!, size: THUltils.setupSize(size: (self.btnTitle.titleLabel?.font.pointSize)!))
    }
    func addBottomLineView()
    {
        bottomLine = UIView(frame: self.frame)
        bottomLine.height = 1
        bottomLine.width  = self.width - 10
        bottomLine.center.x = self.center.x
        bottomLine.bottom = btnTitle.bottom + 1
        bottomLine.backgroundColor = UIColor.black
        self.contentView.addSubview(bottomLine)
    }
    func setLineColor(selected:Bool)
    {
        if(selected)
        {
            bottomLine.backgroundColor = UIColor.white
        }
        else
        {
            bottomLine.backgroundColor = UIColor.black
        }
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
