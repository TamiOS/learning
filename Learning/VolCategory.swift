//
//  VolCategory.swift
//  Learning
//
//  Created by Tam Ho on 11/21/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import Foundation
class VolCategory:SQLTable
{
    var id:Int?
    var imageName:String?
    var content:String?
    init(id:Int,imageName:String,content:String) {
        self.id = id
        self.imageName = imageName
        self.content = content
    }
    
    required init() {
        
    }
    static func getAll() -> [VolCategory]? {
        
        var arrayVol:[VolCategory] = []
        let rows = self.rows()
        for item in rows
        {
            let id = item["id"] as? Int
            let imageName = item["imageName"] as? String
            let content = item["content"] as? String
            
            guard id != nil && imageName != nil && content != nil
                else {
                    return nil
            }
            let model = VolCategory(id: id!, imageName: imageName!, content: content!)
            arrayVol.append(model)
        }
        
        return arrayVol

    }
    
}
