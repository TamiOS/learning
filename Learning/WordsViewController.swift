//
//  WordsViewController.swift
//  Learning
//
//  Created by Tam Ho on 11/29/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
extension WordsViewController:WordTableViewCellDelegate
{
    func didPressFavourite(sender: UIButton) {
        if(self.arrayVol[sender.tag].fav == 0)
        {
            self.arrayVol[sender.tag].setFavourite { (success:Bool) in
                if(success)
                {
                    sender.setBackgroundImage(UIImage(named: "didFav"), for: UIControlState.normal)
                    self.arrayVol[sender.tag].fav = 1
                    self.tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: UITableViewRowAnimation.none)
                }
            }
        }
        else
        {
            self.arrayVol[sender.tag].removeFavourite(completion: { (success:Bool) in
                if(success)
                {
                    sender.setBackgroundImage(UIImage(named: "letFav"), for: UIControlState.normal)
                    self.arrayVol[sender.tag].fav = 0
                    self.tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: UITableViewRowAnimation.none)

                }
            })
        }
        
    }
    func didPressDetail(sender: UIButton) {
        
        
        
    }
}
extension WordsViewController:HVTableViewDelegate,HVTableViewDataSource
{
    func numberOfSections(in tableView: UITableView!) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return arrayVol.count
    }
    func tableView(_ tableView: UITableView!, expand cell: UITableViewCell!, with indexPath: IndexPath!) {
        let cell = cell as? WordTableViewCell
        
        cell?.imgView.alpha = 1
        cell?.imgView.left = MARGIN_SIZE
        cell?.imgView.top = (cell?.containerView.top)!
        
        cell?.labelWord.width = (cell?.containerView.width)!/2
        cell?.labelWord.left = (cell?.imgView.right)! + MARGIN_SIZE
        cell?.labelWord.top = MARGIN_SIZE
        
        cell?.labelPronun.width = (cell?.labelWord.width)!
        cell?.labelPronun.left = (cell?.labelWord.left)!
        cell?.labelPronun.top = (cell?.labelWord.bottom)! + MARGIN_SIZE
        
        cell?.labelPronun.alpha = 1
        cell?.buttonSlowVol.alpha = 1
        cell?.buttonNormalVol.alpha = 1
        
        
        cell?.buttonNormalVol.right = (cell?.containerView.right)! - MARGIN_SIZE * 8
        
        cell?.buttonSlowVol.right = (cell?.buttonNormalVol.left)! - MARGIN_SIZE * 2
        
    
        
    }
    func tableView(_ tableView: UITableView!, collapse cell: UITableViewCell!, with indexPath: IndexPath!) {
        
        let cell = cell as? WordTableViewCell
        cell?.imgView.alpha = 0
        cell?.labelPronun.alpha = 0
        cell?.buttonSlowVol.alpha = 0
        cell?.buttonNormalVol.alpha = 0
        cell?.labelWord.top = (cell?.containerView.height)!/2 - (cell?.labelWord.height)!/2
        cell?.labelWord.left = MARGIN_SIZE
        
    }
    func tableView(_ tableView: UITableView!, didSelectRowAt indexPath: IndexPath!) {
        
    }
    func tableView(_ tableView: UITableView!, cellForRowAt indexPath: IndexPath!, isExpanded: Bool) -> UITableViewCell! {
        var cell:WordTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "WordTableViewCell") as? WordTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("WordTableViewCell", owner: nil, options: nil)?[0] as? WordTableViewCell
        }
        cell?.awakeFromNib()
        cell?.delegate = self
        let vol = arrayVol[indexPath.row]
        cell?.imgView.sd_setImage(with: URL(string: vol.image!))
        cell?.labelWord.text = vol.content
        cell?.labelPronun.text = vol.pronun
        cell?.labelPronun.sizeToFit()
        cell?.labelWord.sizeToFit()
        cell?.buttonNormalVol.titleLabel!.adjustsFontSizeToFitWidth = true
        cell?.buttonSlowVol.titleLabel!.adjustsFontSizeToFitWidth = true
        cell?.buttonFavourite.tag = indexPath.row
        if(arrayVol[indexPath.row].fav == 0)
        {
            cell?.buttonFavourite.setBackgroundImage(UIImage(named: "letFav"), for: UIControlState.normal)
        }
        else
        {
            cell?.buttonFavourite.setBackgroundImage(UIImage(named: "didFav"), for: UIControlState.normal)
        }
        if(!isExpanded)
        {
            cell?.imgView.alpha = 0
            cell?.labelPronun.alpha = 0
            cell?.buttonSlowVol.alpha = 0
            cell?.buttonNormalVol.alpha = 0
            cell?.labelWord.top = (cell?.containerView.height)!/2 - (cell?.labelWord.height)!/2
            cell?.labelWord.left = MARGIN_SIZE
            
        }
        else
        {
            cell?.imgView.alpha = 1
            cell?.imgView.left = MARGIN_SIZE
            cell?.imgView.top = (cell?.containerView.top)!
            
            cell?.labelWord.width = (cell?.containerView.width)!/2
            cell?.labelWord.left = (cell?.imgView.right)! + MARGIN_SIZE
            cell?.labelWord.top = MARGIN_SIZE
            
            cell?.labelPronun.width = (cell?.labelWord.width)!
            cell?.labelPronun.left = (cell?.labelWord.left)!
            cell?.labelPronun.top = (cell?.labelWord.bottom)! + MARGIN_SIZE
            
            cell?.labelPronun.alpha = 1
            cell?.buttonSlowVol.alpha = 1
            cell?.buttonNormalVol.alpha = 1
            
            
            cell?.buttonNormalVol.right = (cell?.containerView.right)! - MARGIN_SIZE * 8
            
            cell?.buttonSlowVol.right = (cell?.buttonNormalVol.left)! - MARGIN_SIZE * 2
            
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView!, heightForRowAt indexPath: IndexPath!, isExpanded: Bool) -> CGFloat {
        if(isExpanded)
        {
            return 160
        }
        return 60
    }
}
class WordsViewController: BaseViewController {

    @IBOutlet weak var btnPhrase: UIButton!
    
    @IBOutlet weak var btnFavourite: UIButton!
    
    @IBOutlet weak var tableView: HVTableView!
    var arrayVol:[Vocabulary] = [Vocabulary]()
    var volCategory:VolCategory!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        // Do any additional setup after loading the view.
    }
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    func setupView()
    {
        
        self.tableView.hvTableViewDelegate = self
        self.tableView.hvTableViewDataSource = self
        self.tableView.backgroundColor = UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 1)
        self.tableView.separatorColor = UIColor.clear
        self.addHeaderView(text: volCategory.content!, isHidden: false)

        getData()
    }
    func getData()
    {
        let vol = Vocabulary.getbyID(type: volCategory.id!)
       if(vol != nil)
       {
            arrayVol = vol!
            self.tableView.reloadData()
       }
    }
   
    
    @IBAction func didPressButton(_ sender: UIButton) {
        
        if(sender.tag == 0)
        {
            let vol = Vocabulary.getbyID(type: volCategory.id!)
            if(vol != nil)
            {
                arrayVol = vol!
                self.tableView.reloadData()
            }
        }
        else
        {
            let vol = Vocabulary.getFavourites(type: volCategory.id!)
            if(vol != nil)
            {
                arrayVol = vol!
                self.tableView.reloadData()
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
