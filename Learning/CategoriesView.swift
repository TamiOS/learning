//
//  CategoriesView.swift
//  Learning
//
//  Created by Tam Ho on 11/8/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
extension CategoriesView:CategoriesTableViewCellDelegate
{
    func didSelectExercise(index:Int)
    {
        delegate?.didSelectItem(index: index)
    }
}
extension CategoriesView:UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayExerciseTypes.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:CategoriesTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "CategoriesTableViewCell") as? CategoriesTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("CategoriesTableViewCell", owner: nil, options: nil)?[0] as? CategoriesTableViewCell
        }
        cell?.buttonCategories.tag = indexPath.row + 1
        cell?.buttonCategories.setTitle(arrayExerciseTypes[indexPath.row].exerciseName, for: UIControlState.normal)
        cell?.delegate = self
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
protocol CategoriesViewDelegate {
    func didCloseView()
    func didSelectItem(index:Int)
}
class CategoriesView: UIView {

    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var buttonClose: UIButton!
    var delegate:CategoriesViewDelegate? = nil
    var arrayExerciseTypes:[ExerciseType] = []
    var idGrammar:Int!
    @IBAction func pressClose(_ sender: UIButton) {
        
        delegate?.didCloseView()
        self.removeFromSuperview()
    }
    override func awakeFromNib() {
        
        self.width = SCREEN_WIDTH
        self.height = THUltils.setupSize(size: self.height)
    }
    func setupView()
    {
        self.tableView.backgroundColor = UIColor.clear
        
        self.tableView.register(UINib(nibName: "CategoriesTableViewCell", bundle: nil), forCellReuseIdentifier: "CategoriesTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        setupData()
    }
    func setupData()
    {
        if let arrayExerciseTypes = ExerciseType.getbyIDGrammar(idGrammar: idGrammar)
        {
            self.arrayExerciseTypes = arrayExerciseTypes
            self.tableView.reloadData()
        }
        
    }
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
