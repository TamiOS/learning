//
//  Type.swift
//  Learning
//
//  Created by Tam Ho on 11/3/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import Foundation
class Type: SQLTable {
    
    var id:Int?
    var typeName:String?
    var idSkill:Int?
    
    init(typeName:String,idSkill:Int) {
        
        self.typeName = typeName
        self.idSkill = idSkill
        
    }
    init(id:Int,typeName:String,idSkill:Int) {
        
        self.id = id
        self.typeName = typeName
        self.idSkill = idSkill
        
    }
    required init() {
    }
    static func getTypebyIdSkill(idSkill:Int) -> [Type]?
    {
        let rows = self.rows(filter: "idSkill = \(idSkill)")
        var arrayType:[Type] = []
        for item in rows
        {
            
            let id = item["id"] as? Int
            let typeName = item["typeName"] as? String
            let idSkill = item["idSkill"] as? Int
            
            guard id != nil && typeName != nil && idSkill != nil else {
                return nil
            }
            let model = Type(id: id!, typeName: typeName!, idSkill: idSkill!)
            arrayType.append(model)
        }
        return arrayType
    }
}
