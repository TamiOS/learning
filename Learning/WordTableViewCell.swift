//
//  WordTableViewCell.swift
//  Learning
//
//  Created by Tam Ho on 11/29/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
protocol WordTableViewCellDelegate {
    func didPressDetail(sender:UIButton)
    func didPressFavourite(sender:UIButton)
}
class WordTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var buttonSlowVol: UIButton!
    
    @IBOutlet weak var buttonNormalVol: UIButton!
    
    @IBOutlet weak var labelWord: UILabel!
    
    @IBOutlet weak var labelPronun: UILabel!
    
    @IBOutlet weak var imgView: UIImageView!
    
    var delegate:WordTableViewCellDelegate? = nil
    var buttonFavourite:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.containerView.layer.cornerRadius = 10
        self.imgView.layer.cornerRadius = 10
        self.backgroundColor = UIColor.clear
        self.buttonSlowVol.titleLabel?.adjustsFontSizeToFitWidth = true
        self.buttonNormalVol.titleLabel?.adjustsFontSizeToFitWidth = true
        
//        let buttonDetail = UIButton(type: UIButtonType.custom)
//        buttonDetail.backgroundColor = UIColor.blue
//        buttonDetail.titleLabel?.text = "Detail"
//        buttonDetail.titleLabel?.sizeToFit()
//        buttonDetail.width = 30
//        buttonDetail.height = 10
//        buttonDetail.center.y = self.containerView.center.y
//        buttonDetail.right = self.containerView.right - MARGIN_SIZE * 8
//        buttonDetail.addTarget(self, action: Selector(("didPressPushDetail:")), for: UIControlEvents.touchUpInside)
//        self.containerView.addSubview(buttonDetail)
        buttonFavourite = UIButton(type: UIButtonType.custom)
        buttonFavourite.setBackgroundImage(UIImage(named: "letFav"), for: UIControlState.normal)
        buttonFavourite.width = 40
        buttonFavourite.height = 35
        buttonFavourite.right = self.containerView.right - MARGIN_SIZE * 8
        buttonFavourite.top = self.containerView.top
        buttonFavourite.addTarget(self, action: #selector(self.didPressFavourite(sender:)), for: UIControlEvents.touchUpInside)
        self.containerView.addSubview(buttonFavourite)
    }
    func didPressFavourite(sender:UIButton)
    {
        delegate?.didPressFavourite(sender: sender)
    }
    func didPressPushDetail(sender:UIButton)
    {
        delegate?.didPressDetail(sender: sender)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func playSlow(_ sender: UIButton) {
    }
    @IBAction func playNormal(_ sender: UIButton) {
    }
    
    
    
}
