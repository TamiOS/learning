//
//  Exercise.swift
//  Learning
//
//  Created by Tam Ho on 11/7/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import Foundation
class Exercise: SQLTable {
    
    var id:Int?
    var contentQuestion:String?
    var idGrammar:Int?
    var idExerciseType:Int?
    required init() {
        
    }
    init(id:Int,contentQuestion:String,idGrammar:Int,idExerciseType:Int) {
        
        self.id = id
        self.contentQuestion = contentQuestion
        self.idGrammar = idGrammar
        self.idExerciseType = idExerciseType
    }
    static func getExercises(idGrammar:Int,idExerciseType:Int = 1) -> [Exercise]?
    {
        var arrayExercise:[Exercise] = []
        let rows = self.rows(filter: "idGrammar = \(idGrammar) and idExerciseType = \(idExerciseType)")
        for item in rows
        {
            let id = item["id"] as? Int
            let contentQuestion = item["contentQuestion"] as? String
            let idGrammar = item["idGrammar"] as? Int
            let idExerciseType = item["idExerciseType"] as? Int
            guard id != nil && contentQuestion != nil && idGrammar != nil && idExerciseType != nil  else {
                return nil
            }
            let model = Exercise(id: id!, contentQuestion: contentQuestion!, idGrammar: idGrammar!,idExerciseType: idExerciseType!)
            arrayExercise.append(model)
        }
        
        return arrayExercise
    }
    
}
