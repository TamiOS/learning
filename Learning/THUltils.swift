//
//  THUltils.swift
//  JackPot
//
//  Created by Tam Ho on 11/2/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import Foundation
import UIKit
class THUltils : UIView
{
    
    static private var textView:UITextView?
    static private var textField:UITextField?
    static func addDoneButtonOnKeyboard(textView:UITextView?,textField:UITextField?)
    {
        self.textView = textView
        self.textField = textField
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(THUltils.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        if textView != nil
        {
            self.textView!.inputAccessoryView = doneToolbar
        }
        if textField != nil
        {
            self.textField!.inputAccessoryView = doneToolbar
        }
        
    }
    static func doneButtonAction()
    {
        if textView != nil
        {
            self.textView!.resignFirstResponder()
        }
        if textField != nil
        {
            self.textField!.resignFirstResponder()
        }
    }
    static func setupSize(size:CGFloat) -> CGFloat
    {
        var sizeDevice = size
        if(IS_IPHONE5)
        {
            sizeDevice = size * 320 / 414
        }
        else if(IS_IPHONE6)
        {
            sizeDevice = size * 375 / 414
        }
        else if(IS_IPHONE4_OR_LESS)
        {
            sizeDevice = size * 480 / 414
        }
        return sizeDevice
    }
    
    
    static func setupSize(subView:[UIView])
    {
        for item in subView
        {
            if let itemTrue = item as? UILabel
            {
                itemTrue.font = UIFont(name: itemTrue.font!.fontName, size:
                    setupSize(size: itemTrue.font!.pointSize))
                itemTrue.adjustsFontSizeToFitWidth = true;
            }
            if let itemBtn = item as? UIButton
            {
                itemBtn.titleLabel!.font = UIFont(name: itemBtn.titleLabel!.font!.fontName, size: setupSize(size: itemBtn.titleLabel!.font!.pointSize))
                itemBtn.titleLabel?.adjustsFontSizeToFitWidth = true;
            }
            if let itemTxt = item as? UITextField
            {
                itemTxt.font = UIFont(name: itemTxt.font!.fontName, size: setupSize(size: itemTxt.font!.pointSize))
                itemTxt.adjustsFontSizeToFitWidth = true;
            }
            item.width = setupSize(size: item.width);
            item.height =  setupSize(size: item.height);
            item.top = setupSize(size: item.top);
            item.left = setupSize(size: item.left);
        }
        
    }
    
    
    
    
}
