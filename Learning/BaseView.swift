//
//  BaseView.swift
//  FrenchLearning
//
//  Created by Tam Ho on 10/27/16.
//  Copyright © 2016 LeeNguyen. All rights reserved.
//

import UIKit

class BaseView: UIView {

    var controller:UIViewController? = nil
    var alertWindow: UIWindow! {
        didSet
        {
            if (alertWindow != nil)
            {
                alertWindow.windowLevel = UIWindowLevelNormal
                alertWindow.backgroundColor = UIColor.clear
                self.center = CGPoint(x: alertWindow.bounds.midX, y: alertWindow.bounds.midY)
                
                alertWindow.addSubview(self)
                
                alertWindow.makeKeyAndVisible()
                
                alertWindow.alpha = 0
                
            }
        }
    };
    override func awakeFromNib() {
        self.height = SCREEN_HEIGHT
        self.width = SCREEN_WIDTH
    }
    /**
     Show Animation
     */
    func showAnimate(_ completion:@escaping () -> Void)
    {
        alertWindow = UIWindow(frame: UIScreen.main.bounds)
        UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
            
            self.alertWindow.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.alertWindow.alpha = 1
        }) { (value: Bool) -> Void in
            completion();
        }
    }
    /**
     Remove Animation
     */
    func removeAnimate(_ completion:@escaping () -> Void)
    {
        UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
            
            self.alertWindow.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.alertWindow.alpha = 0
        }) { (value: Bool) -> Void in
            completion();
        }
    }
    func showAnimateView(_ completion:@escaping () -> Void)
    {
        self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.alpha = 1.0
            self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            completion()
        });
    }
    
    func removeAnimateView(_ completion:@escaping () -> Void)
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    completion()
                    self.removeFromSuperview()
                }
        });
    }
    func popViewController()
    {
        print(controller?.navigationController?.popViewController(animated: true) ?? "Error Pushing")
    }
   
    
    
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
