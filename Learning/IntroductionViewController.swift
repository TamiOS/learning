//
//  IntroductionViewController.swift
//  Learning
//
//  Created by Tam Ho on 11/4/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit

class IntroductionViewController: BaseViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    
    //Constraint
    
    @IBOutlet weak var labelContentHeight: NSLayoutConstraint!
    
    @IBOutlet weak var labelTitleHeight: NSLayoutConstraint!
    
    var grammarModel:Grammar?
    @IBAction func actionSkip(_ sender: UIButton) {
        if grammarModel != nil
        {
            self.navigationController?.pushViewController(CustomSegmentViewController(idGrammar: (grammarModel?.id!)!), animated: true)

        }
        
    }
    init() {
        super.init(nibName: "IntroductionViewController", bundle: nil)
    }
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    override var prefersStatusBarHidden: Bool{
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        // Do any additional setup after loading the view.
    }

    func setupView()
    {
        if let grammar = grammarModel
        {
            self.labelTitle.text = grammar.nameLesson
            self.labelContent.text = grammar.summaryContent
            self.addHeaderView(text: grammar.nameLesson!, isHidden: false)
        }
        self.btnSkip.titleLabel?.font = UIFont(name: self.btnSkip.titleLabel!.font.fontName, size: THUltils.setupSize(size: (self.btnSkip.titleLabel?.font.pointSize)!))!
        self.labelTitle.font = UIFont(name: self.labelTitle.font.fontName, size: THUltils.setupSize(size: self.labelTitle.font.pointSize))
        self.labelContent.font = UIFont(name: self.labelContent.font.fontName, size: THUltils.setupSize(size: self.labelContent.font.pointSize))
        self.labelContent.sizeToFit()
        labelContentHeight.constant = self.labelContent.height + MARGIN_SIZE * 3
        self.view.layoutIfNeeded()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
