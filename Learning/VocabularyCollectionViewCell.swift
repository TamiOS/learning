//
//  VocabularyCollectionViewCell.swift
//  Learning
//
//  Created by Tam Ho on 11/21/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit

class VocabularyCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var imageVol: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.cornerRadius = 10
        // Initialization code
    }

}
