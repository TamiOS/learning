//
//  TabExerciseCollectionViewCell.swift
//  Learning
//
//  Created by Tam Ho on 11/6/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit

class TabExerciseCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelAnswer: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 10
        // Initialization code
    }
    func setBorderColor(selected:Bool)
    {
        if(selected)
        {
            self.layer.borderColor = UIColor.blue.cgColor
        }
        else
        {
            self.layer.borderColor = UIColor.gray.cgColor
        }
    }
    

}
