//
//  Constant.swift
//  LearningEng
//
//  Created by Tam Ho on 10/23/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import Foundation
import UIKit

let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
let MARGIN_SIZE:CGFloat = 8

let KEYBOARD_HEIGHT:CGFloat = 216
let KEYBOARD_ANIMATE_DURATION = 0.3

let DEVICE_VERSION = UIDevice.current.systemVersion

let IS_IPAD = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)
let IS_IPHONE = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone)
let IS_RETINA = UIScreen.main.scale > 2.0

let SCREEN_MAX_LENGTH = max(SCREEN_WIDTH,SCREEN_HEIGHT)
let SCREEN_MIN_LENGTH = min(SCREEN_WIDTH,SCREEN_HEIGHT)

let IS_IPHONE4_OR_LESS = (IS_IPHONE && SCREEN_MAX_LENGTH < 568)
let IS_IPHONE5 = (IS_IPHONE && SCREEN_MAX_LENGTH == 568)
let IS_IPHONE6 = (IS_IPHONE && SCREEN_MAX_LENGTH == 667)
let IS_IPHONE6PLUS = (IS_IPHONE && SCREEN_MAX_LENGTH == 736)

let GRAMMAR = 1
let VOCABULARY = 2
let TESTING = 3

let BACKGROUND_GRAMMAR = UIColor(red: 255/255, green: 212/255, blue: 149/255, alpha: 1)
let BACKGROUND_VOCABULARY = UIColor(red: 255/255, green: 245/255, blue: 89/255, alpha: 1)
let BACKGROUND_TESTING = UIColor(red: 253/255, green: 195/255, blue: 255/255, alpha: 1)
let MULTIPLE_CHOICE_TYPE = 1
let QUESTION_TYPE = 2

