//
//  AnswerView.swift
//  Learning
//
//  Created by Tam Ho on 11/7/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
extension MutableCollection where Index == Int {
    /// Shuffle the elements of `self` in-place.
    mutating func shuffle() {
        // empty and single-element collections don't shuffle
        if count < 2 { return }
        
        for i in startIndex ..< endIndex - 1 {
            let j = Int(arc4random_uniform(UInt32(endIndex - i))) + i
            if i != j {
                swap(&self[i], &self[j])
            }
        }
    }
}
extension Collection {
    /// Return a copy of `self` with its elements shuffled
    func shuffled() -> [Iterator.Element] {
        var list = Array(self)
        list.shuffle()
        return list
    }
}
extension AnswerView : DropDownViewDelegate
{
    func didPressChange() {
        
        let categoryView = CategoriesView.loadFromNibNamed("CategoriesView") as! CategoriesView
        categoryView.delegate = self
        categoryView.idGrammar = exercise.idGrammar
        categoryView.setupView()
        parentViewController.scrollView.isScrollEnabled = false
        self.addSubview(categoryView)
    }
}
extension AnswerView : CategoriesViewDelegate
{
    func didCloseView() {
        
        parentViewController.scrollView.isScrollEnabled = true
    }
    func didSelectItem(index: Int) {
        
        parentViewController.reloadData(idExercise: index)
    }
    
}
extension AnswerView : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayAnswers.count
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell:TabExerciseCollectionViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "TabExerciseCollectionViewCell", for: indexPath) as? TabExerciseCollectionViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("TabExerciseCollectionViewCell", owner: nil, options: nil)?[0] as? TabExerciseCollectionViewCell
        }
        cell?.layer.borderColor = UIColor.gray.cgColor
        cell?.labelAnswer.text = arrayAnswers[indexPath.row].content
        cell?.labelAnswer.sizeToFit()
        cell?.labelAnswer.width = (cell?.width)! - MARGIN_SIZE * 2
        cell?.labelAnswer.left = MARGIN_SIZE
        cell?.labelAnswer.top = MARGIN_SIZE
        cell?.height = (cell?.labelAnswer.bottom)! + MARGIN_SIZE
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.width - MARGIN_SIZE * 3)/2, height: (collectionView.height - MARGIN_SIZE * 3)/2)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return MARGIN_SIZE
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(MARGIN_SIZE, MARGIN_SIZE, MARGIN_SIZE, MARGIN_SIZE);
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! TabExerciseCollectionViewCell
        for item in collectionView.visibleCells
        {
            item.layer.borderColor = UIColor.gray.cgColor
        }
        cell.layer.borderColor = UIColor.blue.cgColor
    }
    
}
extension AnswerView:ResultViewDelegate
{
    func didPressAgain() {
        
        let currentPage =  Int(parentViewController.scrollView.contentOffset.y / parentViewController.scrollView.height)
        parentViewController.arrayQuestion[currentPage] = false
        collectionView.reloadData()
    }
}
protocol AnswerViewDelegate {
    func didPressOK()
}
class AnswerView: UIView {

    
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var btnOK: UIButton!
    
    @IBOutlet weak var labelQuestion: UILabel!
    
    var delegate:AnswerViewDelegate? = nil
    var arrayAnswers:[Answer] = []
    var exercise:Exercise!
    var parentViewController:TabExerciseViewController!
    var currentIndex:Int = 1
    var numberPage = 1
    override func awakeFromNib() {
        self.width = SCREEN_WIDTH
        self.height = THUltils.setupSize(size: self.height)
        
        
    }
    @IBAction func actionOK(_ sender: UIButton) {
        
        var arrayIndex = collectionView.indexPathsForSelectedItems
        if(arrayIndex?.count != 0)
        {
            let index = arrayIndex?[0].row
            if(arrayAnswers[index!].isTrue == 1)
            {
                print("True")
                let cell = collectionView.cellForItem(at: (arrayIndex?[0])!) as! TabExerciseCollectionViewCell

                for item in collectionView.visibleCells
                {
                    item.layer.borderColor = UIColor.gray.cgColor
                }
                cell.layer.borderColor = UIColor.blue.cgColor
            }
            else
            {
                print("false")
                for (index,item) in arrayAnswers.enumerated()
                {
                    
                    if(item.isTrue == 1)
                    {
                        let cell = collectionView.cellForItem(at: IndexPath(row: index, section: 0)) as! TabExerciseCollectionViewCell
                        for item in collectionView.visibleCells
                        {
                            item.layer.borderColor = UIColor.gray.cgColor
                        }
                        cell.layer.borderColor = UIColor.red.cgColor
                    }
                }
                let resultView = ResultView.loadFromNibNamed("ResultView") as! ResultView
                resultView.frame = self.contentView.frame
                resultView.delegate = self
                self.contentView.addSubview(resultView)
            }
            let currentPage =  Int(parentViewController.scrollView.contentOffset.y / parentViewController.scrollView.height)
            parentViewController.arrayQuestion[currentPage] = true
            if(arrayAnswers[index!].isTrue == 1)
            {
                parentViewController.arrayAnswer[currentPage] = true
            }
            else
            {
                parentViewController.arrayAnswer[currentPage] = false
            }
            delegate?.didPressOK()
        }
        
    }
    
    func setupView(exercise:Exercise)
    {
        let indicatorDot = UIPageControl(frame: self.btnOK.frame)
        indicatorDot.backgroundColor = UIColor.clear
        indicatorDot.pageIndicatorTintColor = btnOK.titleLabel?.textColor
        indicatorDot.currentPageIndicatorTintColor = UIColor.gray
        indicatorDot.height = btnOK.height/2
        indicatorDot.top = btnOK.bottom
        indicatorDot.numberOfPages = numberPage
        indicatorDot.currentPage = currentIndex
        self.contentView.addSubview(indicatorDot)
        self.exercise = exercise
        self.collectionView.register(UINib(nibName: "TabExerciseCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TabExerciseCollectionViewCell")
        self.collectionView.backgroundColor = UIColor.clear
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.height = 90
        self.collectionView.bottom = btnOK.top - MARGIN_SIZE
        self.collectionView.width = self.width - MARGIN_SIZE * 2
        self.collectionView.left = MARGIN_SIZE
        addDropDownView()
        setupData()
    }
    func setupData()
    {

        if let arrayAnswers = Answer.getAnswer(idExercise: self.exercise.id!)
        {
            labelQuestion.text = exercise.contentQuestion
            labelQuestion.sizeToFit()
            labelQuestion.width = self.width - MARGIN_SIZE * 2
            customizeLabel()
            self.arrayAnswers = arrayAnswers.shuffled()
            
            self.collectionView.reloadData()
        }
    }
    func addDropDownView()
    {
        let dropDown = DropDownView.loadFromNibNamed("DropDownView") as! DropDownView
        
        dropDown.delegate = self
        dropDown.frame = self.btnOK.frame
        dropDown.layer.borderColor = UIColor.black.cgColor
        dropDown.layer.borderWidth = 1
        dropDown.layer.cornerRadius = 10
        dropDown.top = MARGIN_SIZE
        self.contentView.addSubview(dropDown)
        self.labelQuestion.top = dropDown.bottom + MARGIN_SIZE
        self.labelQuestion.width = self.collectionView.width
        self.labelQuestion.left = self.collectionView.left
    }
    func customizeLabel()
    {
        labelQuestion.isHidden = true
        let arrayText:[String] = (labelQuestion.text?.components(separatedBy: "_"))!
        let constantLeft = labelQuestion.left + MARGIN_SIZE
        let constantTop = labelQuestion.top + MARGIN_SIZE * 2

        var left = constantLeft
        var top = constantTop
        var height:CGFloat = 0
        var right:CGFloat = 0
        var bottom:CGFloat = 0
        let viewQuestion = UIView(frame: btnOK.frame)
        viewQuestion.top = labelQuestion.top

        self.contentView.addSubview(viewQuestion)

        for (index,item) in arrayText.enumerated()
        {
            
            let labelText = UILabel()
            labelText.numberOfLines = 0
            labelText.font = UIFont(name: labelQuestion.font.fontName, size: labelQuestion.font.pointSize)
            labelText.text = item
            labelText.sizeToFit()
            if(right + labelText.intrinsicContentSize.width > labelQuestion.right)
            {
                left = constantLeft
                top  = bottom
            }
            labelText.left = left
            labelText.top = top
            left = labelText.right + MARGIN_SIZE
            top = labelText.top
            self.contentView.addSubview(labelText)
            if(left + 60 >= labelQuestion.right)
            {
                left = constantLeft
                top  = bottom
            }
            if(index != arrayText.count - 1)
            {
                let spaceView = UIView()
                spaceView.layer.borderColor = UIColor.black.cgColor
                spaceView.layer.borderWidth = 1
                spaceView.layer.cornerRadius = 8
                spaceView.width = 60
                spaceView.height = labelText.height
                spaceView.top = top
                spaceView.left = left
                left = spaceView.right + MARGIN_SIZE
                self.contentView.addSubview(spaceView)
                right = spaceView.right + MARGIN_SIZE
                bottom = spaceView.bottom + MARGIN_SIZE
            }
            height = labelText.bottom
        }
        viewQuestion.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        viewQuestion.layer.cornerRadius = 10
        viewQuestion.left = MARGIN_SIZE
        viewQuestion.height = height - viewQuestion.top + MARGIN_SIZE * 2
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
