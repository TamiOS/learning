//
//  UIView+Coords.swift
//  GiftCardSwift
//
//  Created by Huu Nam's Mac Pro on 3/27/15.
//  Copyright (c) 2015 Huu Nam's Mac Pro. All rights reserved.
//

import UIKit
/// A first-in/first-out queue of unconstrained size
/// - Complexity: push is O(1), pop is O(`count`)
public struct Queue<T>: ExpressibleByArrayLiteral {
    /// backing array store
    public fileprivate(set) var elements: Array<T> = []
    
    /// introduce a new element to the queue in O(1) time
    public mutating func push(_ value: T) { elements.append(value) }
    
    /// remove the front of the queue in O(`count` time
    public mutating func pop() -> T { return elements.removeFirst() }
    
    /// test whether the queue is empty
    public var isEmpty: Bool { return elements.isEmpty }
    
    /// queue size, computed property
    public var count: Int { return elements.count }
    
    /// offer `ArrayLiteralConvertible` support
    public init(arrayLiteral elements: T...) { self.elements = elements }
}
public typealias VoidBlock = ((Void) -> Void)
public extension UINavigationController
{
    public func pushViewController(_ viewController: UIViewController, animated: Bool, completion: @escaping VoidBlock) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.pushViewController(viewController, animated: animated)
        CATransaction.commit()
    }
}

extension UIImageView
{
    func fadeImage(_ image:UIImage)
    {
        let toImage = image
        UIView.transition(with: self,
                                  duration:5,
                                  options: .transitionCrossDissolve,
                                  animations: { self.image = toImage },
                                  completion: nil)
    }
    
}



extension UIViewController {
    
    func animateThing(_ thing: UIView, offsetY: CGFloat, offsetX: CGFloat, time: TimeInterval, delay: TimeInterval) {
        let targetY = thing.center.y
        let targetX = thing.center.x
        
        thing.center.y = thing.center.y - offsetY
        thing.center.x = thing.center.x - offsetX
        
        
        UIView.animate(withDuration: time, delay: delay, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
            thing.center.y = targetY
            thing.center.x = targetX
            }, completion: nil)
    }
}

extension Collection where Index: Comparable {
    subscript (safe index: Index) -> Iterator.Element? {
        guard startIndex <= index && index < endIndex else {
            return nil
        }
        return self[index]
    }
}
extension UIViewController {
    var className: String {
        return NSStringFromClass(self.classForCoder).components(separatedBy: ".").last!
    }
}
extension UIViewController {
    func showAlert(title: String, message: String, handler: ((UIAlertAction) -> Swift.Void)? = nil) {
        DispatchQueue.main.async { [unowned self] in
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: handler))
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
extension Array {
    subscript (safe index: UInt) -> Element? {
        return Int(index) < count ? self[Int(index)] : nil
    }
}

extension UIView
{
    class func loadFromNibNamed(_ nibNamed: String, bundle : Bundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
    
    var left:CGFloat
        {
        set(x)
        {
            var frame:CGRect=self.frame
            frame.origin.x=x
            self.frame=frame
        }
        get
        {
            return self.frame.origin.x
        }
        
    }
    
    
    var top:CGFloat
        {
        set(y)
        {
            var frame:CGRect=self.frame
            frame.origin.y=y
            self.frame=frame
        }
        get
        {
            return self.frame.origin.y
        }
    }
    
    
    var width:CGFloat
        {
        set(width)
        {
            var frame:CGRect=self.frame
            frame.size.width = width
            self.frame = frame
        }
        get
        {
            return self.frame.size.width
        }
    }
    
    var height:CGFloat
        {
        set(height)
        {
            var frame:CGRect=self.frame
            frame.size.height = height
            self.frame = frame
        }
        get
        {
            return self.frame.size.height
        }
    }
    
    var right:CGFloat
        {
        set(right)
        {
            var frame:CGRect=self.frame
            frame.origin.x = right-self.width
            self.frame = frame
        }
        get
        {
            return self.frame.origin.x + self.frame.size.width
        }
    }
    
    
    var bottom:CGFloat
        {
        set(bottom)
        {
            var frame:CGRect=self.frame
            frame.origin.y = bottom-self.height
            self.frame = frame
        }
        get
        {
            return self.frame.origin.y + self.frame.size.height
        }
    }
    
}
