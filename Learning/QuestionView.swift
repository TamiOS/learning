//
//  QuestionView.swift
//  Learning
//
//  Created by Tam Ho on 11/11/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit
extension QuestionView:ResultViewDelegate
{
    func didPressAgain() {
        
        let currentPage =  Int(parentViewController.scrollView.contentOffset.y / parentViewController.scrollView.height)
        parentViewController.arrayQuestion[currentPage] = false
        collectionView.reloadData()
    }
}
extension QuestionView:CategoriesViewDelegate
{
    func didCloseView() {
        
        parentViewController.scrollView.isScrollEnabled = true
    }
    func didSelectItem(index: Int) {
        
        parentViewController.reloadData(idExercise: index)
    }
}
extension QuestionView:DropDownViewDelegate
{
    func didPressChange() {
        
        let categoryView = CategoriesView.loadFromNibNamed("CategoriesView") as! CategoriesView
        categoryView.delegate = self
        categoryView.idGrammar = self.exercise.idGrammar
        categoryView.setupView()
        parentViewController.scrollView.isScrollEnabled = false
        self.addSubview(categoryView)
    }
}
extension QuestionView:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayAnswers.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell:QuestionCollectionViewCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "QuestionCollectionViewCell", for: indexPath) as? QuestionCollectionViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("QuestionCollectionViewCell", owner: nil, options: nil)?[0] as? QuestionCollectionViewCell
        }
        cell?.awakeFromNib()
        cell?.labelQuestion.text = arrayAnswers[indexPath.row].content
        cell?.labelQuestion.sizeToFit()
        cell?.labelQuestion.width = (cell?.width)! - MARGIN_SIZE * 4
        cell?.labelQuestion.left = MARGIN_SIZE * 2
        cell?.labelQuestion.top = MARGIN_SIZE * 2
        cell?.height = (cell?.labelQuestion.bottom)! + MARGIN_SIZE * 2
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width - 16, height: 80)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        let cell = collectionView.cellForItem(at: indexPath) as! QuestionCollectionViewCell
        for item in collectionView.visibleCells
        {
            item.layer.borderColor = UIColor.gray.cgColor
        }
        cell.layer.borderColor = UIColor.blue.cgColor

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return MARGIN_SIZE
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: MARGIN_SIZE, left: MARGIN_SIZE, bottom: 0, right: MARGIN_SIZE)
    }
    
}
class QuestionView: UIView {

    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var btnOK: UIButton!
    
    @IBOutlet weak var contentView: UIView!
    
    var delegate:AnswerViewDelegate? = nil
    var arrayAnswers:[Answer] = []
    var exercise:Exercise!
    var parentViewController:TabExerciseViewController!
    var currentIndex:Int = 1
    var numberPage = 1

    @IBAction func doOK(_ sender: UIButton) {
        
        var arrayIndex = collectionView.indexPathsForSelectedItems
        if(arrayIndex?.count != 0)
        {
            let index = arrayIndex?[0].row
            if(arrayAnswers[index!].isTrue == 1)
            {
                print("True")
                let cell = collectionView.cellForItem(at: (arrayIndex?[0])!) as! QuestionCollectionViewCell
                
                for item in collectionView.visibleCells
                {
                    item.layer.borderColor = UIColor.gray.cgColor
                }
                cell.layer.borderColor = UIColor.blue.cgColor
            }
            else
            {
                print("false")
                for (index,item) in arrayAnswers.enumerated()
                {
                    
                    if(item.isTrue == 1)
                    {
                        let cell = collectionView.cellForItem(at: IndexPath(row: index, section: 0)) as! QuestionCollectionViewCell
                        for item in collectionView.visibleCells
                        {
                            item.layer.borderColor = UIColor.gray.cgColor
                        }
                        cell.layer.borderColor = UIColor.red.cgColor
                    }
                }
                let resultView = ResultView.loadFromNibNamed("ResultView") as! ResultView
                resultView.frame = self.contentView.frame
                resultView.delegate = self
                self.contentView.addSubview(resultView)
            }
            let currentPage =  Int(parentViewController.scrollView.contentOffset.y / parentViewController.scrollView.height)
            parentViewController.arrayQuestion[currentPage] = true
            if(arrayAnswers[index!].isTrue == 1)
            {
                parentViewController.arrayAnswer[currentPage] = true
            }
            else
            {
                parentViewController.arrayAnswer[currentPage] = false
            }
            delegate?.didPressOK()
        }
    }
    func setupView()
    {
        print(self.frame)
        print(self.contentView.frame)
        let indicatorDot = UIPageControl(frame: self.btnOK.frame)
        indicatorDot.backgroundColor = UIColor.clear
        indicatorDot.pageIndicatorTintColor = btnOK.titleLabel?.textColor
        indicatorDot.currentPageIndicatorTintColor = UIColor.gray
        indicatorDot.height = btnOK.height/2
        indicatorDot.top = btnOK.bottom
        indicatorDot.numberOfPages = numberPage
        indicatorDot.currentPage = currentIndex
        self.contentView.addSubview(indicatorDot)
        self.collectionView.width = self.contentView.width - MARGIN_SIZE
        self.collectionView.register(UINib(nibName: "QuestionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "QuestionCollectionViewCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.backgroundColor = UIColor.clear
        self.collectionView.height = 300
        setupData()
        addDropDownView()
    }
    func setupData()
    {
        if let arrayAnswers = Answer.getAnswer(idExercise: self.exercise.id!)
        {
            self.arrayAnswers = arrayAnswers.shuffled()
            
            self.collectionView.reloadData()
        }
        
    }
    func addDropDownView()
    {
        
        let dropDown = DropDownView.loadFromNibNamed("DropDownView") as! DropDownView
        dropDown.delegate = self
        dropDown.frame = self.btnOK.frame
        dropDown.layer.borderColor = UIColor.black.cgColor
        dropDown.layer.borderWidth = 1
        dropDown.layer.cornerRadius = 10
        dropDown.top = MARGIN_SIZE
        self.contentView.addSubview(dropDown)
        let labelQuestion = UILabel(frame: self.contentView.frame)
        labelQuestion.left = MARGIN_SIZE
        labelQuestion.top = dropDown.bottom + MARGIN_SIZE
        labelQuestion.width = self.contentView.width
        labelQuestion.adjustsFontSizeToFitWidth = true
        labelQuestion.text = self.exercise.contentQuestion
        labelQuestion.sizeToFit()
        self.contentView.addSubview(labelQuestion)
        self.collectionView.top = labelQuestion.bottom + MARGIN_SIZE
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
