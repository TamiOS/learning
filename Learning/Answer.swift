//
//  Answer.swift
//  Learning
//
//  Created by Tam Ho on 11/7/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import Foundation
class Answer: SQLTable {
    
    var id:Int?
    var content:String?
    var idExercise:Int?
    var isTrue:Int?
    
    required init() {
        
    }
    init(content:String,idExercise:Int,isTrue:Int) {
        
        self.content = content
        self.idExercise = idExercise
        self.isTrue = isTrue
    }
    init(id:Int,content:String,idExercise:Int,isTrue:Int) {
        self.id = id
        self.content = content
        self.idExercise = idExercise
        self.isTrue = isTrue
    }
    static func getAnswer(idExercise:Int) -> [Answer]?
    {
        var arrayAnswer:[Answer] = []
        let rows = self.rows(filter: "idExercise = \(idExercise)")
        for item in rows
        {
            let id = item["id"] as? Int
            let content = item["content"] as? String
            let idExercise = item["idExercise"] as? Int
            let isTrue = item["isTrue"] as? Int
            
            guard id != nil && content != nil && idExercise != nil && isTrue != nil else {
                return nil
            }
            let model = Answer(id: id! , content: content!, idExercise: idExercise!, isTrue: isTrue!)
            arrayAnswer.append(model)
        }
        
        return arrayAnswer
    }
}
