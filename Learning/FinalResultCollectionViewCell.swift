//
//  FinalResultCollectionViewCell.swift
//  Learning
//
//  Created by Tam Ho on 11/11/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit

class FinalResultCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var labelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        self.layer.cornerRadius = 10
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 1
        THUltils.setupSize(subView: self.contentView.subviews)
        // Initialization code
    }

}
