//
//  BaseViewController.swift
//  Learning
//
//  Created by Tam Ho on 11/4/16.
//  Copyright © 2016 Tam Ho. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    var headerView:HeaderView = HeaderView.loadFromNibNamed("HeaderView") as! HeaderView
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    func addHeaderView(text:String,isHidden:Bool)
    {
        headerView.setupView()
        headerView.controller = self
        headerView.setText(text: text,isHidden: isHidden)
        self.view.addSubview(headerView)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
